/**
 * Created by Platzhalter on 04.01.2016
 */
module.exports = exports = function(vars) {
    var trustedClients = vars.trustedClients;
    var websiteTitle = vars.websiteTitle;
    var client = vars.client;

    return function (from, message) {
        var trusted = false;
        if(trustedClients.hasOwnProperty(from)) if(trustedClients[from]) trusted = true;

        var msg = message.split(' ');
        switch(msg[0]) {
            case "join":
                if(!trusted) { client.say(from, "You are not allowed to use this command."); break; }
                if(msg.length < 2) { client.say(from, "Syntax: join [channelname]"); break; }
                client.join(msg[1], function() {
                    client.say(from, "Joined " + msg[1]);
                });
                break;
            case "part":
                if(!trusted) { client.say(from, "You are not allowed to use this command."); break; }
                if(msg.length < 2) { client.say(from, "Syntax: part [channelname]"); break; }
                client.part(msg[1], function() {
                    client.say(from, "Parted from " + msg[1]);
                });
                break;
            case "say":
                if(msg.length < 3) { client.say(from, "Syntax: say [channelname] [message]"); break; }
                var fullmsg = "";
                for(var i = 2; i < msg.length; i++) {
                    fullmsg += msg[i] + " ";
                }
                client.say(msg[1], (trusted ? fullmsg : from + " requested me to say: \"" + fullmsg + "\""));
                client.say(from, "I said that.");
                break;
            case "websitetitle":
                if(!trusted) { client.say(from, "You are not allowed to use this command."); break; }
                if(msg.length < 3) { client.say(from, "Syntax: websitetitle [ADD|REM] [channelname]"); break; }
                switch(msg[1].toLowerCase()) {
                    case "add":
                        websiteTitle[msg[2]] = true;
                        break;
                    case "rem":
                        websiteTitle[msg[2]] = false;
                        break;
                }
                break;
            case "trust":
                if(!trusted) { client.say(from, "You are not allowed to use this command."); break; }
                if(msg.length < 3) { client.say(from, "Syntax: trust [ADD|REM] [nickname]"); break; }
                switch(msg[1].toLowerCase()) {
                    case "add":
                        trustedClients[msg[2]] = true;
                        break;
                    case "rem":
                        trustedClients[msg[2]] = false;
                        break;
                }
                break;
            case "reload":
                if(!trusted) { client.say(from, "You are not allowed to use this command."); break; }
                client.say(from, "Reloading... Please wait.");
                vars.reload(from);
                break;
            case "command":
                if(!trusted) { client.say(from, "You are not allowed to use this command."); break; }
                if(msg.length < 3) { client.say(from, "Syntax: command [command] [arguments]"); break; }
                var cmd = "";
                for(var i = 1; i < msg.length; i++) {
                    cmd += msg[i] + " ";
                }
                client.conn.write('cmd\r\n');
                break;
            case "register":
                if(!trusted) { client.say(from, "You are not allowed to use this command."); break; }
                client.say('nickserv', 'register PASSWORD this@is-fa.ke');
                client.say(from, 'Okay.');
                break;
            case "login":
                if(!trusted) { client.say(from, "You are not allowed to use this command."); break; }
                client.say('nickserv', 'identify PASSWORD');
                client.say(from, 'Okay.');
                break;
            case "nick":
                if(!trusted) { client.say(from, "You are not allowed to use this command."); break; }
                if(msg.length < 2) { client.say(from, "Syntax: nick [nick]"); break; }
                client.send('nick', msg[1]);
                break;
        }
    }

};