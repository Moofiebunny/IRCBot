/**
 * Created by Platzhalter on 04.01.2016
 */

module.exports = exports = function (vars) {
    var trustedClients = vars.trustedClients;
    var client = vars.client;

    String.prototype.replaceAt = function(index, character) {
        return this.substr(0, index) + character + this.substr(index+character.length);
    };

    function clone(obj) {
        // Handle the 3 simple types, and null or undefined
        if (null == obj || "object" != typeof obj) return obj;

        // Handle Date
        if (obj instanceof Date) {
            var copy = new Date();
            copy.setTime(obj.getTime());
            return copy;
        }

        // Handle Array
        if (obj instanceof Array) {
            var copy = [];
            for (var i = 0, len = obj.length; i < len; i++) {
                copy[i] = clone(obj[i]);
            }
            return copy;
        }

        // Handle Object
        if (obj instanceof Object) {
            var copy = {};
            for (var attr in obj) {
                if (obj.hasOwnProperty(attr)) copy[attr] = clone(obj[attr]);
            }
            return copy;
        }

        throw new Error("Unable to copy obj! Its type isn't supported.");
    }

    var games = [
        {
            "name": "Hangman",
            "message": function(from, to, message) {
                if(vars.activeGame.word) {
                    var word = vars.activeGame.word.toLowerCase();
                    if(message.length == 1) {
                        var letter = message.toLowerCase();
                        var contains = false;
                        for(var l = 0; l < word.length; l++) {
                            if(word[l] == letter) {
                                contains = true;
                                vars.activeGame.hiddenWord = vars.activeGame.hiddenWord.replaceAt(l, vars.activeGame.word[l]);
                            }
                        }

                        var won = true;
                        for(var i = 0; i < vars.activeGame.hiddenWord.length; i++) {
                            if (vars.activeGame.hiddenWord[i] == "_") won = false;
                        }

                        if(won) {
                            client.say(to, "YOU WON THE GAME!\nThe word was: \n\"" + vars.activeGame.word + "\"");
                            delete vars.activeGame;
                            vars.activeGame = null;
                        }
                        else if(contains) client.say(to, "YES!\n" + vars.activeGame.hiddenWord);
                        else {
                            vars.activeGame.mistakes++;
                            client.say(to, vars.activeGame.mistake[vars.activeGame.mistakes]);
                            if(vars.activeGame.mistakes == 6) {
                                client.say(to, "You lost... :(\nThe word was:\n\"" + vars.activeGame.word + "\"");
                                delete vars.activeGame;
                                vars.activeGame = null;
                            }
                        }
                    }
                }
            },
            "pm": function(from, message) {
                if(from == vars.activeGame.leader && !vars.activeGame.word) {
                    for(var i = 0; i < message.length; i++) {
                        if(message[i] == "_") {
                            client.say(from, "This is not a valid word.");
                            return;
                        }
                    }
                    vars.activeGame.word = message;
                    vars.activeGame.hiddenWord = message.replace(/[a-zA-Z0-9]/g, "_");
                    client.say(vars.activeGame.channel, "The word is: " + vars.activeGame.hiddenWord);
                }
            },
            "start": function() {
                client.say(vars.activeGame.leader, "Please send me your word!");
            },
            "word": null,
            "hiddenWord": null,
            "channel": null,
            "mistakes": 0,
            "ident": Math.random(),
            "mistake": {
                1:
                "__________ \n" +
                "|         |\n" +
                "|         0\n" +
                "|          \n" +
                "|          \n" +
                "|          \n",
                2:
                "__________ \n" +
                "|         |\n" +
                "|         0\n" +
                "|         |\n" +
                "|          \n" +
                "|          \n",

                3:
                "__________ \n" +
                "|         |\n" +
                "|         0\n" +
                "|        /|\n" +
                "|          \n" +
                "|          \n",

                4:
                "__________ \n" +
                "|         |\n" +
                "|         0\n" +
                "|        /|\\\n" +
                "|          \n" +
                "|          \n",

                5:
                "__________ \n" +
                "|         |\n" +
                "|         0\n" +
                "|        /|\\\n" +
                "|        / \n" +
                "|          \n",

                6:
                "__________ \n" +
                "|         |\n" +
                "|         0\n" +
                "|        /|\\\n" +
                "|        / \\\n" +
                "|          \n"
            },
            "description":  "A game in which one player chooses a word,\n" +
                            "animal, movie title, etc. The name of the\n" +
                            "'secret phrase' is not given to other\n" +
                            "players, but one blank for each letter of\n" +
                            "the 'secret phrase' is. Spaces are shown,\n" +
                            "too. The other players have a certain amount\n" +
                            "of turns to guess what the phrase is, one\n" +
                            "letter at a time. When a letter that is not\n" +
                            "in the secret phrase is guessed, a body part\n" +
                            "(starting with the head, continuing with the\n" +
                            "body, two arms, and two legs) is drawn on a\n" +
                            "hangman's noose (I don't know what the stand\n" +
                            "is called, but the noose is where the head is\n" +
                            "paced). When the entire body is complete, the\n" +
                            "player who picked the secret phrase wins. If\n" +
                            "the phrase is guessed before all body parts\n" +
                            "are hung, the others win."
        }
        ];

    var aliases = { };

    return {
        "message": function (from, to, message) {
            var k;

            if (message[0] == '.') {
                var cmd = message.split('.')[1].split(' ');
                switch (cmd[0].toLowerCase()) {
                    case "game":
                        if (cmd.length > 1) {
                            if(vars.activeGame == null) {
                                for (k = 0; k < games.length; k++) {
                                    if (games[k].name.toLowerCase() == cmd[1]) {
                                        vars.activeGame = clone(games[k]);
                                        vars.activeGame.leader = from;
                                        vars.activeGame.channel = to;
                                        vars.activeGame.start();
                                        return;
                                    }
                                }
                                client.say(to, "This game does not exist.\n" +
                                    "Please use .LIST to list all available games.");
                            } else client.say(to, "There's already a running game: " + vars.activeGame.name);
                        } else client.say(to, "Please use this command as following: .GAME [name]\n" +
                            "To list all available games, please do .LIST");
                        break;
                    case "list":
                        client.say(to, "Available games are:");
                        for (k = 0; k < games.length; k++) {
                            client.say(to, "- " + games[k].name);
                        }
                        break;
                    case "info":
                        if (cmd.length > 1) {
                            for (k = 0; k < games.length; k++) {
                                if (games[k].name.toLowerCase() == cmd[1]) {
                                    client.say(to, games[k].description);
                                    return;
                                }
                            }
                            client.say(to, "This game does not exist.\n" +
                                "Please use .LIST to list all available games.");
                        } else client.say(to, "Please use this command as following:\n" +
                            ".INFO [name]");
                        break;
                }
            } else {
                if(vars.activeGame != null) vars.activeGame.message(from, to, message);
            }

            if (aliases[to]) {
                aliases[to](from, message.split(' '));
            }
        },
        "pm": function(from, message) {
            var trusted = false;
            if(trustedClients.hasOwnProperty(from)) if(trustedClients[from]) trusted = true;

            var msg = message.split(' ');
            switch(msg[0]) {
                case "join":
                    if(!trusted) { client.say(from, "You are not allowed to use this command."); break; }
                    if(msg.length < 2) { client.say(from, "Syntax: join [channelname]"); break; }
                    client.join(msg[1], function() {
                        client.say(from, "Joined " + msg[1]);
                    });
                    break;
                case "part":
                    if(!trusted) { client.say(from, "You are not allowed to use this command."); break; }
                    if(msg.length < 2) { client.say(from, "Syntax: part [channelname]"); break; }
                    client.part(msg[1], function() {
                        client.say(from, "Parted from " + msg[1]);
                    });
                    break;
                case "trust":
                    if(!trusted) { client.say(from, "You are not allowed to use this command."); break; }
                    if(msg.length < 3) { client.say(from, "Syntax: trust [ADD|REM] [nickname]"); break; }
                    switch(msg[1].toLowerCase()) {
                        case "add":
                            trustedClients[msg[2]] = true;
                            break;
                        case "rem":
                            trustedClients[msg[2]] = false;
                            break;
                    }
                    break;
                case "reload":
                    if(!trusted) { client.say(from, "You are not allowed to use this command."); break; }
                    client.say(from, "Reloading... Please wait.");
                    vars.reload(from);
                    break;
                case "register":
                    if(!trusted) { client.say(from, "You are not allowed to use this command."); break; }
                    client.say('nickserv', 'register passw0rd0991 this@is-fa.ke');
                    client.say(from, 'Okay.');
                    break;
                case "login":
                    if(!trusted) { client.say(from, "You are not allowed to use this command."); break; }
                    client.say('nickserv', 'identify passw0rd0991');
                    client.say(from, 'Okay.');
                    break;
                case "nick":
                    if(!trusted) { client.say(from, "You are not allowed to use this command."); break; }
                    if(msg.length < 2) { client.say(from, "Syntax: nick [nick]"); break; }
                    client.send('nick', msg[1]);
                    break;
                case "stopgames":
                    if(trusted) vars.activeGame = null;
                    break;
                default:
                    if(vars.activeGame != null) {
                        vars.activeGame.pm(from, message);
                    }
                    break;
            }
        }
    };
};