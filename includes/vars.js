/**
 * Created by Platzhalter on 04.01.2016
 */
module.exports = exports = function (irc) {
    var r = {
        irc: irc,

        client: new irc.Client('109.169.67.138', 'Gamezz', {
            password: null,
            userName: 'Gamezz',
            realName: 'Gamezz',
            port: 6697,
            localAddress: null,
            debug: true,
            showErrors: true,
            autoRejoin: true,
            autoConnect: true,
            channels: ['#lounge'],
            retryCount: null,
            retryDelay: 2000,
            secure: false,
            selfSigned: true,
            certExpired: false,
            floodProtection: false,
            floodProtectionDelay: 0,
            sasl: false,
            stripColors: false,
            channelPrefixes: '#',
            messageSplit: 512,
            encoding: false,
            webirc: {
                pass: '',
                ip: '',
                user: ''
            }
        }),

        trustedClients: {
            'Platzhalter': true
        },
        websiteTitle: {
        },
        activeGame: null
    };

    r.listener = {
        message: require('./listeners/message')(r).message,
        pm: require('./listeners/message')(r).pm,
        error: require('./listeners/error')
    };

    return r;
};