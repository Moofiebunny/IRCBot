/**
 * Created by Platzhalter on 04.01.2016
 */
var irc = require('irc');
var vars = require('./includes/vars')(irc);

vars.reload = function(from) {
    vars.client.removeListener('message', vars.listener.message);
    vars.client.removeListener('pm', vars.listener.pm);
    delete require.cache[require.resolve('./includes/listeners/message')];
    vars.listener.message = require('./includes/listeners/message')(vars).message;
    vars.client.addListener('message', vars.listener.message);

    vars.listener.pm = require('./includes/listeners/message')(vars).pm;
    vars.client.addListener('pm', vars.listener.pm);

    vars.client.removeListener('error', vars.listener.error);
    delete require.cache[require.resolve('./includes/listeners/error')];
    vars.listener.error = require('./includes/listeners/error');
    vars.client.addListener('error', vars.listener.error);

    vars.client.say(from, "Finished.");
};

vars.client.addListener('message', vars.listener.message);
vars.client.addListener('pm', vars.listener.pm);
vars.client.addListener('error', vars.listener.error);